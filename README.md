# Mischgas
Visualisierung von vieldimensionalen Gasmischverhältnissen in R, erster Versuch

## Was wird benötigt?
- Laufzeitumgebung der statistischen Programmiersprache R -> [Download](https://cran.r-project.org/)
- optional, aber empfohlen: Entwicklungsumgebung RStudio -> [Download](https://posit.co/download/rstudio-desktop/)
- Basisdaten als Exceldatei (XLSX-Format; hier nicht im Repo enthalten)

## Einstiegsliteratur
- Nicht allumfassend, aber ausreichend: [Einführung in R](https://amd-lab.github.io/R-Kurs-Buch/), hier ist z. B.
    - [Kapitel 5](https://amd-lab.github.io/R-Kurs-Buch/datenaufbereitung-mit-dplyr.html) für das Verständnis der im Skript angewendeten Datenverarbeitung mittels Pipes (%>%) und
    - [Kapitel 7](https://amd-lab.github.io/R-Kurs-Buch/graphiken-mit-ggplot2.html#ggplot2-einf%C3%BChrung) über die Visualisierung mit dem ggplot2-Paket hilfreich.

